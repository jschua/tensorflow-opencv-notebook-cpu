FROM  gcr.io/kubeflow-images-public/tensorflow-2.0.0a0-notebook-cpu:v0.7.0 AS base

LABEL maintainer="Chua Jie Sheng <hello@jschua.com>"

USER root

RUN apt-get update && apt-get upgrade -y && apt-get install -y qt4-qmake libqt4-dev cmake python3-opencv git
RUN git clone --recursive https://github.com/skvark/opencv-python.git
RUN export CMAKE_ARGS="-DOPENCV_ENABLE_NONFREE=ON" && \
    export ENABLE_CONTRIB=1 && \
    cd opencv-python && \
    python3 setup.py bdist_wheel && \
    pip3 install dist/opencv_contrib_python-*.whl && \
    cd .. && rm -r opencv-python

WORKDIR /opencv
COPY sift_check.py /opencv
RUN python3 sift_check.py

COPY requirements.txt /opencv
RUN pip3 install -r requirements.txt

EXPOSE 8888
USER jovyan
ENTRYPOINT ["tini", "--"]
CMD ["sh","-c", "jupyter notebook --notebook-dir=/home/${NB_USER} --ip=0.0.0.0 --no-browser --allow-root --port=8888 --NotebookApp.token='' --NotebookApp.password='' --NotebookApp.allow_origin='*' --NotebookApp.base_url=${NB_PREFIX}"]